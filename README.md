# CWMIB Wordassistant

Get the length of the word from the user

Filter out all words with the given length

Ask the user if they have identified any letter present in the word

If yes, get the letter and the index position of the letter. 

Filter out the words containing the letter in the same index position

Ask the user if they have found any letter not present in the word

Filter out words that do not contain these letters