const fs = require('fs');
const readline = require('readline-sync');


function readInput(){
    let words;
    try {
        const data = fs.readFileSync('en_dict.txt', 'utf8');
        words = data.replace(/\'/g,"").split('\n');
      } catch (err) {
        console.error(err);
      }
      uniques = words.filter((x, i) => i === words.indexOf(x))
      return(uniques);
}

function getWordsbyLength(words, length)
{
    const new_array = words.filter((element) => {
        return element.length == length;
    });
    return new_array;
}

String.prototype.replaceAt = function(word, index, replacement) {
    return word.substring(0, index) + replacement + word.substring(index + 1);
}

function getWordsMatching(words, letter, index){
    let filteredWords;
    if(index == -1){
        filteredWords = words.filter((element) => {
            return element.includes(letter);
        });
    }
    else{
        filteredWords = words.filter((element) => {
            return element[index] == letter;
        });
    }
    return filteredWords;
}
function getWordsNotMatching(words, letter){
    const filteredWords = words.filter((element) => {
        return !element.includes(letter);
    });
    return filteredWords;
}

function getInitialWord(wordlength){
    let word = ""
    for (var i=0; i< wordlength; i++){
         word = word + '$'
    }
}

let words = readInput()
//let words = ['apple', 'mango', 'banana', 'orange']
let wordlength = readline.question("Wordlength: ")
final_words = getWordsbyLength(words, wordlength);
word = getInitialWord(wordlength)
option = readline.question("Did you find a letter?: ")
while(option == 'Y' || option == 'y'){
    letter = readline.question("letter: ")
    index = parseInt(readline.question("position(Enter -1 if you don't know the position): "))
    console.log(index);
   // word = word.replaceAt(word, index, letter)
    final_words = getWordsMatching(final_words, letter, index)
    option = readline.question("Did you find another letter?: ")
}

option = readline.question("Did you find a letter not present?: ")
while(option == 'Y' || option == 'y'){
    letter = readline.question("letter: ")
    final_words = getWordsNotMatching(final_words, letter, index)
    option = readline.question("Did you find another letter not present?: ")
}

console.log(final_words);

